<?php
class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);
    }

    // Достаем строку запроса из глобальной переменной $_SERVER
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI']))
        {
            return (trim($_SERVER['REQUEST_URI'], '/'));
        }
    }

    public function run()
    {
        // Получить строку запроса
        $uri = $this->getURI();

        // Проверить наличие такого запроса в routes.php
        foreach ($this->routes as $uriPattern => $path)
        {
//            echo $uriPattern . "<br>";
            // Если есть совпадение, определить какой контроллер и action обрабатывает запрос
            if (preg_match("~$uriPattern~", $uri))
            {
//                echo '<br>Где ищем (запрос, который набрал пользователь): ' . $uri;
//                echo '<br>Что ищем (совпадение из правила): ' . $uriPattern;
//                echo '<br>Кто обрабатывает: '. $path . "<br>";
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
//                echo $internalRoute . "<br>";
                $segments = explode('/', $internalRoute);

//                echo '<br><br>';
//                print_r($segments);
//                echo '<br><br>';
                $controllerName = array_shift($segments).'Controller';
                $controllerName = ucfirst($controllerName);
//                echo "Название контролера:" . $controllerName . "<br>";
                $actionName = 'action'.ucfirst(array_shift($segments));
//                echo "Название екшена:" . $actionName . "<br>";
                $parameters = $segments;
//                Подключить файл класса-контроллера
                $controllerFile = ROOT.'/controllers/' . $controllerName . '.php';

                if (file_exists($controllerFile))
                {
                    include_once($controllerFile);
                }

                $controllerObject = new $controllerName;
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                if ($result != NULL)
                {
                    break ;
                }
            }
        }
    }
}
