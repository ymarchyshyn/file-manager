function get_filetype(mime_type)
{
    switch (mime_type)
    {
        case "directory":
            return "directory";
        case "image/png":
        case "image/pjpeg":
        case "image/jpg":
        case "image/jpeg":
        case "image/gif":
        case "image/bmp":
        case "image/x-ms-bmp":
        case "image/x-windows-bmp":
            return "image";
        default:
            return "file";
    }
}

function file_icon(mime_type)
{
    switch (mime_type)
    {
        case "directory":
            return "fileicon fas fa-folder fa-fw";
        case "image/png":
        case "image/pjpeg":
        case "image/jpg":
        case "image/jpeg":
        case "image/gif":
        case "image/bmp":
        case "image/x-ms-bmp":
        case "image/x-windows-bmp":
            return "fileicon far fa-file-image fa-fw";
        default:
            return "fileicon far fa-file fa-fw";
    }
}

function readImage(file)
{
    var xhr = new XMLHttpRequest();

    xhr.open("POST", "/api/get/image", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("file=" + file);

    xhr.onreadystatechange = function() {

        if (xhr.readyState === 4 && xhr.status === 200 && xhr.responseText) {

            var image = new Image();
            image.src = this.responseText;

            var newTab = window.open();

            newTab.document.body.innerHTML = '<img src="'+ image.src + '">';
        }
    }
}

function clearOutput()
{
    var output = document.getElementById("files-output");

    output.innerHTML = "";
}

function getPath(file)
{
    var path = document.getElementById("location").innerText;

    if (file === "..") {
        var index = path.lastIndexOf("/");
        if (path !== "/" && index !== -1) {
            path = index === 0 ?  "/" : path.substr(0, index);
        }
        return path;
    }

    return path + (path[path.length - 1] !== "/" ? "/" : "") + file;
}

function onClick(filename, filetype)
{
    var newPath = getPath(filename);

    switch (filetype)
    {
        case "directory": {
            getFilesAndRender(newPath);
            clearOutput();
            break ;
        }
        case "image":
            readImage(newPath);
            break ;
        default:
            break;
    }
}

function renderOnCallback(xhr)
{
    if (xhr.readyState === 4 && xhr.status === 200 && xhr.responseText)
    {
        var output = document.getElementById("files-output");
        var files = JSON.parse(xhr.responseText);

        document.getElementById("location").innerText = files[0]['path'];

        files.splice(0, 1);

        files.forEach(function(file, item) {

            var icon = file_icon(file.mime_type);
            var elem = document.createElement('div');
            var filename = file.filename + (file.mime_type === 'denied' ? ' <i class="fs-12">(permission denied)</i>' : '');

            elem.classList.add('row');
            elem.setAttribute('data-file', file.filename);
            elem.setAttribute('data-filetype', get_filetype(file.mime_type));
            elem.innerHTML = "<div class='double-col'>" +
                                "<div class='col-wrapper'>" +
                                    "<div class='dib'>" +
                                        "<i class='" + icon + "'></i>" +
                                    "</div>" +
                                        "<div class='dib pl-2'>" + filename + "</div>" +
                                    "</div>" +
                                "</div>" +
                                "<div class='filesize-col col'>" + file.size + "</div>" +
                            "</div>";

            output.appendChild(elem);

            elem.addEventListener('click', function(e) {
                var clicked = e.currentTarget;
                var filename = clicked.getAttribute('data-file');
                var filetype = clicked.getAttribute('data-filetype');

                onClick(filename, filetype);
            });
        });

        output.style.display = "block";
    }
}

function getFilesAndRender(path)
{
    var xhr = new XMLHttpRequest();

    xhr.open("POST", "/api/read", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("path=" + path);

    xhr.onreadystatechange = function() {
        renderOnCallback(this);
    }
}

document.addEventListener('DOMContentLoaded', function() {

    var is_root = location.pathname === "/";

    if (is_root) {

        var path = document.getElementById("location").innerText;
        getFilesAndRender(path);
    }

});