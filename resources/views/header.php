<section class="header container">
    <div class="header-wrapper">
        <div class="company-logo">
            <a href="/">
                <h1>File Manager</h1>
            </a>
        </div>
        <div class="search-form">
            <input type="text" placeholder="Enter filename..."/>
            <button class="search-button">
                <i class="fas fa-fw fa-search"></i>
            </button>
        </div>
    </div>
</section>