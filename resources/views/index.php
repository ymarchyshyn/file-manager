<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>File Manager</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/styles.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>
<body>

    <!-- HEADER -->
    <?php require_once('header.php'); ?>
    <!-- HEADER -->

    <section class="file-manager container pt-4 pb-5">

        <?php if (isset($currentDir)): ?>
            <div>
                <p class="dib">Location: </p>
                <p id="location" class="dib pb-3"><?php echo $currentDir; ?></p>
            </div>
        <?php endif; ?>

        <div class="files-table">
            <div class="row">
                <div class="double-col">
                    Filename
                </div>
                <div class="filesize-col-title col">
                    Size
                </div>
            </div>
            <div id="files-output" style="display: none"></div>
        </div>

    </section>
    <script type="text/javascript" src="/resources/js/scripts.js"></script>
</body>
