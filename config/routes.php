<?php
    return array(
        '^api/read$' => '/read',
        '^api/get/image$' => '/image',
        '^$' => '/index',
    );