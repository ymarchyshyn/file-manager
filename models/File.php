<?php

class File
{
    protected static function getMimeType($path)
    {
        if (!$mimeType = @mime_content_type($path)) {
            return "denied";
        }
        else {
            return $file['mime_type'] = $mimeType;
        }
    }

    public static function getImage64($path)
    {
        if ($data = file_get_contents($path)) {
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            return $base64;
        }
        return 0;
    }

    public static function readDir($path)
    {
        if (is_dir($path)) {
            if ($handle = opendir($path)) {
                while (false !== ($entry = readdir($handle))) {
                    $pathToFile = $path . '/' . $entry;

                    $file = [
                        'filename' => $entry,
                        'size' => filesize($pathToFile),
                        'mime_type' => self::getMimeType($pathToFile)
                    ];

                    $files[] = $file;
                }
            }

            if ($files[0]['filename'] === ".") {
                array_shift($files);
            }

            array_unshift($files, [
              'path' => $path,
            ]);

            sort($files);
            return $files;
        }
        else {
            // File is not a directory
            return 0;
        }
    }
}