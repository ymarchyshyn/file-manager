<?php

class Controller
{
    public function actionRead()
    {
        $path = $_POST['path'];

        $files = File::readDir($path);

        echo json_encode($files);
    }

    public function actionImage()
    {
        $file = $_POST['file'];

        $result = File::getImage64($file);

        echo $result;
    }

    public function actionIndex()
    {
        $currentDir = getcwd();

        require_once(ROOT . '/resources/views/index.php');
    }
}